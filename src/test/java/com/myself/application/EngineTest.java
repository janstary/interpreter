package com.myself.application;

import com.myself.application.interpreter.FileProcessor;
import com.myself.application.interpreter.exception.CompileErrorException;
import com.myself.application.interpreter.exception.SyntaxErrorException;
import com.myself.application.interpreter.impl.DefaultEngine;
import com.myself.application.interpreter.model.Expression;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EngineTest {

    /**
     * A program that calculates the factorial of the specified variable
     * i, it puts into the faktorial variable and writes it to the screen.
     *
     * @throws FileNotFoundException when input file is not found
     */
    @Test
    public void test_01_factorial_success() throws FileNotFoundException {
        // GIVEN
        String filename = "src/test/resources/data1.txt";
        DefaultEngine engine = getEngine("src/test/resources/input/test1.txt");
        List<Expression> expressions = FileProcessor.of(filename)
                                                    .readFile();

        // WHEN
        engine.addAll(expressions)
              .process();

        // THEN
        assertNotNull(engine.getVariables().get("faktorial"));
        assertEquals(120, (int) engine.getVariables().get("faktorial"));
    }

    /**
     * Similar to the first example, but on the line 4 is an instruction to jump to line 9
     * that does not exist, so the program must report an error.
     *
     * @throws FileNotFoundException when input file is not found
     */
    @Test
    public void test_02_failure() throws FileNotFoundException {
        // GIVEN
        String filename = "src/test/resources/data2.txt";
        DefaultEngine engine = getEngine("src/test/resources/input/test1.txt");
        List<Expression> expressions = FileProcessor.of(filename)
                                                    .readFile();

        // WHEN
        SyntaxErrorException thrown = assertThrows(SyntaxErrorException.class,
                                                   () -> engine.addAll(expressions)
                                                               .process(),
                                                   "Expected process() to throw, but it did not");

        // THEN
        assertNotNull(thrown);
        assertEquals("Syntax error on line 4. The line number 8 is out of range.",
                     thrown.getMessage());
        assertTrue(!thrown.getMessage().isEmpty());
    }

    /**
     * Program, which for the entered number into the variable vstup
     * will store -1, if negative, 0 if it is even and 1 if it is odd
     * and puts the variable status on the screen.
     * <p>
     * input = 5
     *
     * @throws FileNotFoundException when input file is not found
     */
    @Test
    public void test_03_success() throws FileNotFoundException {
        // GIVEN
        String filename = "src/test/resources/data3.txt";
        DefaultEngine engine = getEngine("src/test/resources/input/test1.txt");
        List<Expression> expressions = FileProcessor.of(filename)
                                                    .readFile();

        // WHEN
        engine.addAll(expressions)
              .process();

        // THEN
        assertNotNull(engine.getVariables().get("status"));
        assertEquals(1, (int) engine.getVariables().get("status"));
    }

    /**
     * Program, which for the entered number into the variable vstup
     * will store -1, if negative, 0 if it is even and 1 if it is odd
     * and puts the variable status on the screen.
     * <p>
     * input = 10
     *
     * @throws FileNotFoundException when input file is not found
     */
    @Test
    public void test_04_success() throws FileNotFoundException {
        // GIVEN
        String filename = "src/test/resources/data3.txt";
        DefaultEngine engine = getEngine("src/test/resources/input/test2.txt");
        List<Expression> expressions = FileProcessor.of(filename)
                                                    .readFile();

        // WHEN
        engine.addAll(expressions)
              .process();

        // THEN
        assertNotNull(engine.getVariables().get("status"));
        assertEquals(0, (int) engine.getVariables().get("status"));
    }

    /**
     * Program, which for the entered number into the variable vstup
     * will store -1, if negative, 0 if it is even and 1 if it is odd
     * and puts the variable status on the screen.
     * <p>
     * input = -5
     *
     * @throws FileNotFoundException when input file is not found
     */
    @Test
    public void test_05_success() throws FileNotFoundException {
        // GIVEN
        String filename = "src/test/resources/data3.txt";
        DefaultEngine engine = getEngine("src/test/resources/input/test3.txt");
        List<Expression> expressions = FileProcessor.of(filename)
                                                    .readFile();

        // WHEN
        engine.addAll(expressions)
              .process();

        // THEN
        assertNotNull(engine.getVariables().get("status"));
        assertEquals(-1, (int) engine.getVariables().get("status"));
    }

    /**
     * A program similar to task 3 but in the penultimate instruction there is a typo
     * and non-existent variable statusq - the interpreter must report a bug!
     *
     * @throws FileNotFoundException when input file is not found
     */
    @Test
    public void test_06_failure() throws FileNotFoundException {
        // GIVEN
        String filename = "src/test/resources/data4.txt";
        DefaultEngine engine = getEngine("src/test/resources/input/test1.txt");
        List<Expression> expressions = FileProcessor.of(filename)
                                                    .readFile();

        // WHEN
        SyntaxErrorException thrown = assertThrows(SyntaxErrorException.class,
                                                   () -> engine.addAll(expressions)
                                                               .process(),
                                                   "Expected process() to throw, but it did not");

        // THEN
        assertNotNull(thrown);
        assertEquals("Syntax error on line 15, near 'WRITE,statusq'. The variable statusq does not exists!",
                     thrown.getMessage());
        assertTrue(!thrown.getMessage().isEmpty());
    }

    /**
     * A program similar to task 3, but the third instruction is a bug
     * and a non-existent variable vacsi - the interpreter must report a bug!
     *
     * @throws FileNotFoundException when input file is not found
     */
    @Test
    public void test_07_failure() throws FileNotFoundException {
        // GIVEN
        String filename = "src/test/resources/data5.txt";
        DefaultEngine engine = getEngine("src/test/resources/input/test1.txt");
        List<Expression> expressions = FileProcessor.of(filename)
                                                    .readFile();

        // WHEN
        SyntaxErrorException thrown = assertThrows(SyntaxErrorException.class,
                                                   () -> engine.addAll(expressions)
                                                               .process(),
                                                   "Expected process() to throw, but it did not");

        // THEN
        assertNotNull(thrown);
        assertEquals("Syntax error. The literal 'vacsi' is neither variable, nor number.",
                     thrown.getMessage());
        assertTrue(!thrown.getMessage().isEmpty());
    }

    /**
     * Wrong syntax, variable name starts with number
     */
    @Test
    public void test_08_failure() {
        // GIVEN
        String filename = "src/test/resources/data6.txt";

        // WHEN
        CompileErrorException thrown = assertThrows(CompileErrorException.class,
                                                    () -> FileProcessor.of(filename)
                                                                       .readFile(),
                                                    "Expected readFile() to throw, but it did not");

        // THEN
        assertNotNull(thrown);
        assertEquals("Compile error near 'READ 5premenna'. The syntax is incorrect.",
                     thrown.getMessage());
        assertTrue(!thrown.getMessage().isEmpty());
    }

    /**
     * Wrong syntax, variable name starts with number
     */
    @Test
    public void test_09_failure() {
        // GIVEN
        String filename = "src/test/resources/data7.txt";

        // WHEN
        CompileErrorException thrown = assertThrows(CompileErrorException.class,
                                                    () -> FileProcessor.of(filename)
                                                                       .readFile(),
                                                    "Expected readFile() to throw, but it did not");

        // THEN
        assertNotNull(thrown);
        assertEquals("Compile error near 'WRITE 5premenna'. The syntax is incorrect.",
                     thrown.getMessage());
        assertTrue(!thrown.getMessage().isEmpty());
    }

    public DefaultEngine getEngine(String input) throws FileNotFoundException {
        return DefaultEngine.of(new FileReader(input));
    }

}
