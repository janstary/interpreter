package com.myself.application.interpreter;

import com.myself.application.interpreter.exception.CouldNotProcessFileException;
import com.myself.application.interpreter.model.Expression;

import java.io.IOException;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static java.lang.String.format;
import static java.nio.file.Files.lines;
import static java.nio.file.Paths.get;
import static java.util.stream.Collectors.toList;

/**
 * Class for processing the file
 */
public class FileProcessor {

    /**
     * File to be read
     */
    private final String filename;

    private FileProcessor(String filename) {
        this.filename = filename;
    }

    public static FileProcessor of(String filename) {
        return new FileProcessor(filename);
    }

    /**
     * @return list of expressions
     */
    public List<Expression> readFile() {
        try (Stream<String> lines = lines(get(this.filename))) {

            Predicate<String> isEmpty = String::isEmpty;
            Predicate<String> notEmpty = isEmpty.negate();

            return lines.filter(notEmpty)
                        .map(String::trim)
                        .map(ExpressionFactory::getExpression)
                        .collect(toList());

        } catch (IOException e) {
            throw new CouldNotProcessFileException(
                    format("Could not open a file '%s'", this.filename), e);
        }
    }

}
