package com.myself.application.interpreter.model;

/**
 * The enum represents one special type of instruction. For example WRITE, READ,
 * JUMP, '*' as MULTIPLICATION and so on.
 */
public enum Keyword {
    WRITE,
    READ,
    ADDITION,
    SUBTRACTION,
    MULTIPLICATION,
    GREATER_THAN,
    LESS_THAN,
    GREATER_THAN_OR_EQUAL,
    LESS_THAN_OR_EQUAL,
    EQUAL_TO,
    ASSIGN,
    JUMP,
    JUMPT,
    JUMPF,
    NOP
}
