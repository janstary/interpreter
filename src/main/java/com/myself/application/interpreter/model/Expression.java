package com.myself.application.interpreter.model;

import java.util.function.BiFunction;

/**
 * The class represents one line in the source code.
 */
public final class Expression {

    /**
     * Type of the expression
     */
    private final Keyword keyword;

    /**
     * Represents value of the one line of the source code. So in another word, it is raw,
     * unparsed string literal.
     */
    private final String value;

    /**
     * Represents a specific operation to be done.
     */
    private final BiFunction<Integer, Integer, Integer> operation;

    public Expression(Keyword keyword,
                      String value,
                      BiFunction<Integer, Integer, Integer> operation) {
        this.keyword = keyword;
        this.value = value;
        this.operation = operation;
    }

    public static ExpressionBuilder builder() {return new ExpressionBuilder();}

    public Keyword getKeyword() {
        return keyword;
    }

    public String getValue() {
        return value;
    }

    public BiFunction<Integer, Integer, Integer> getOperation() {
        return operation;
    }

    public static final class ExpressionBuilder {

        private Keyword keyword;
        private String value;
        private BiFunction<Integer, Integer, Integer> operation;

        private ExpressionBuilder() {
        }

        public ExpressionBuilder keyword(Keyword keyword) {
            this.keyword = keyword;
            return this;
        }

        public ExpressionBuilder value(String value) {
            this.value = value;
            return this;
        }

        public ExpressionBuilder operation(BiFunction<Integer, Integer, Integer> operation) {
            this.operation = operation;
            return this;
        }

        public Expression build() {
            return new Expression(keyword, value, operation);
        }
    }
}
