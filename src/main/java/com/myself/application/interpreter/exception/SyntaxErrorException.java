package com.myself.application.interpreter.exception;

/**
 * Exception is thrown when the error occurs at run time. For example when the variable
 * which should be printed does not exist.
 */
public class SyntaxErrorException extends RuntimeException {

    public SyntaxErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public SyntaxErrorException(String message) {
        super(message);
    }
}
