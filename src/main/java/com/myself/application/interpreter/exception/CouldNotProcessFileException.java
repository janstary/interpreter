package com.myself.application.interpreter.exception;

/**
 * Exception is thrown when the file cannot be opened and processed.
 */
public class CouldNotProcessFileException extends RuntimeException {

    public CouldNotProcessFileException(String message, Throwable cause) {
        super(message, cause);
    }

    public CouldNotProcessFileException(String message) {
        super(message);
    }
}
