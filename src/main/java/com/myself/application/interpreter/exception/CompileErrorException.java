package com.myself.application.interpreter.exception;

/**
 * Exception is thrown when the error occurs at compile time, in another word,
 * when the source code cannot be processed.
 */
public class CompileErrorException extends RuntimeException {

    public CompileErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public CompileErrorException(String message) {
        super(message);
    }
}
