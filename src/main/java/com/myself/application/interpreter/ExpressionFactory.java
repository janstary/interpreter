package com.myself.application.interpreter;

import com.myself.application.interpreter.exception.CompileErrorException;
import com.myself.application.interpreter.model.Expression;

import static com.myself.application.interpreter.model.Keyword.*;
import static java.lang.String.format;

/**
 * Factory class for creating Expression instances according to input string.
 */
public class ExpressionFactory {

    /**o
     * Based on the patterns applied to the input string, it creates Expression instances with
     * different parameters.
     *
     * @param input string representing one line in source code
     * @return Expression
     * @throws CompileErrorException when none of patterns matches the input
     */
    public static Expression getExpression(final String input) throws CompileErrorException {
        if (input.matches("(WRITE),([a-zA-Z].*)")) {
            return new Expression(WRITE, input, null);
        } else if (input.matches("(READ),([a-zA-Z].*)")) {
            return new Expression(READ, input, null);
        } else if (input.matches("(\\+),(.+),(.+),(.+)")) {
            return new Expression(ADDITION, input, (i, j) -> i + j);
        } else if (input.matches("(-),(.+),(.+),(.+)")) {
            return new Expression(SUBTRACTION, input, (i, j) -> i - j);
        } else if (input.matches("(\\*),(.+),(.+),(.+)")) {
            return new Expression(MULTIPLICATION, input, (i, j) -> i * j);
        } else if (input.matches("(>),(.+),(.+),(.+)")) {
            return new Expression(GREATER_THAN, input, (i, j) -> (i > j) ? 1 : 0);
        } else if (input.matches("(<),(.+),(.+),(.+)")) {
            return new Expression(LESS_THAN, input, (i, j) -> (i < j) ? 1 : 0);
        } else if (input.matches("(>=),(.+),(.+),(.+)")) {
            return new Expression(GREATER_THAN_OR_EQUAL, input, (i, j) -> (i >= j) ? 1 : 0);
        } else if (input.matches("(<=),(.+),(.+),(.+)")) {
            return new Expression(LESS_THAN_OR_EQUAL, input, (i, j) -> (i <= j) ? 1 : 0);
        } else if (input.matches("(==),(.+),(.+),(.+)")) {
            return new Expression(EQUAL_TO, input, (i, j) -> (i.equals(j)) ? 1 : 0);
        } else if (input.matches("(=),(.+),(.+)")) {
            return new Expression(ASSIGN, input, null);
        } else if (input.matches("(JUMP),(\\d+)")) {
            return new Expression(JUMP, input, null);
        } else if (input.matches("(JUMPT),(.+),(\\d+)")) {
            return new Expression(JUMPT, input, null);
        } else if (input.matches("(JUMPF),(.+),(\\d+)")) {
            return new Expression(JUMPF, input, null);
        } else if (input.matches("NOP")) {
            return new Expression(NOP, input, null);
        }
        throw new CompileErrorException(
                format("Compile error near '%s'. The syntax is incorrect.", input));
    }

}
