package com.myself.application.interpreter;

import java.util.Collection;

/**
 * Interface represents engine responsible for executing the expressions
 *
 * @param <T>
 */
public interface Engine<T> {

    /**
     * @param entity to be processed
     * @return Engine
     */
    Engine<T> add(T entity);

    /**
     * @param entities collection of entities to be processed
     * @return Engine
     */
    Engine<T> addAll(Collection<T> entities);

    /**
     * Takes list of Expressions and based on the keyword performs the specific functionality.
     */
    void process();

}
