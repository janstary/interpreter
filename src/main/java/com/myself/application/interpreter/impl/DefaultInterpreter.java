package com.myself.application.interpreter.impl;

import com.myself.application.interpreter.FileProcessor;
import com.myself.application.interpreter.Interpreter;
import com.myself.application.interpreter.exception.CompileErrorException;
import com.myself.application.interpreter.exception.CouldNotProcessFileException;
import com.myself.application.interpreter.exception.SyntaxErrorException;
import com.myself.application.interpreter.model.Expression;

import java.io.InputStreamReader;
import java.util.List;

/**
 * Class represents an interpreter
 */
public class DefaultInterpreter implements Interpreter {

    public final String filename;

    private DefaultInterpreter(String filename) {
        this.filename = filename;
    }

    public static DefaultInterpreter of(String filename) {
        return new DefaultInterpreter(filename);
    }

    public void interpret() {
        try {
            List<Expression> expressions = FileProcessor.of(this.filename)
                                                        .readFile();

            DefaultEngine.of(new InputStreamReader(System.in))
                         .addAll(expressions)
                         .process();

        } catch (CompileErrorException | SyntaxErrorException | CouldNotProcessFileException e) {
            System.err.println(e.getMessage());
        }
    }
}
