package com.myself.application.interpreter.impl;

import com.myself.application.interpreter.Engine;
import com.myself.application.interpreter.exception.SyntaxErrorException;
import com.myself.application.interpreter.model.Expression;

import java.io.Reader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;

import static java.lang.Integer.parseInt;
import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Optional.ofNullable;

/**
 * Basic implementation if engine interface.
 */
public class DefaultEngine implements Engine<Expression> {

    /**
     * Map for storing all variables
     */
    private final Map<String, Integer> variables = new HashMap<>();

    /**
     * Expressions to be processed
     */
    private final List<Expression> expressions = new ArrayList<>();

    /**
     * Source of input
     */
    private final Reader in;

    private DefaultEngine(Reader in) {
        this.in = in;
    }

    public static DefaultEngine of(Reader in) {
        return new DefaultEngine(in);
    }

    public Map<String, Integer> getVariables() {
        return variables;
    }

    /**
     * Adds new Expression to expressions list
     *
     * @param expression expression to add
     * @return DefaultEngine
     */
    @Override
    public DefaultEngine add(Expression expression) {
        this.expressions.add(expression);
        return this;
    }

    /**
     * Adds all expressions to expressions
     *
     * @param expressions List of expression to add
     * @return DefaultEngine
     */
    @Override
    public DefaultEngine addAll(Collection<Expression> expressions) {
        this.expressions.addAll(expressions);
        return this;
    }

    /**
     * Takes list of Expressions and based on the keyword performs the specific functionality.
     **/
    @Override
    public void process() {
        final int length = this.expressions.size();

        for (int i = 0; i < length; i++) {
            Expression expression = this.expressions.get(i);
            List<String> literals = getLiterals(expression);

            switch (expression.getKeyword()) {
                case WRITE:
                    writeVariable(literals, expression, i + 1);
                    break;
                case READ:
                    readVariable(literals);
                    break;
                case ADDITION:
                case SUBTRACTION:
                case MULTIPLICATION:
                case GREATER_THAN:
                case LESS_THAN:
                case GREATER_THAN_OR_EQUAL:
                case LESS_THAN_OR_EQUAL:
                case EQUAL_TO:
                    provideOperation(literals, expression);
                    break;
                case ASSIGN:
                    assignVariable(literals);
                    break;
                case JUMP:
                    int jumpValue = getIntValueOrThrow(literals.get(1));
                    if (jumpValue - 1 > length - 1) { // -1 because the list starts at 0 index
                        throw new SyntaxErrorException(
                                format("Syntax error on line %s. The line number %s is out of range.",
                                       i + 1,
                                       jumpValue - 1));
                    }
                    i = jumpValue - 2; // -1 because of for loop and another -1 because the list starts at 0 index
                    break;
                case JUMPT:
                    i = provideJump(literals, true, length, i);
                    break;
                case JUMPF:
                    i = provideJump(literals, false, length, i);
                    break;
                case NOP:
                    break;
            }
        }
    }

    /**
     * Splits the Expression`s value by comma.
     *
     * @param expression represents one specific Expression
     * @return list of string
     */
    private List<String> getLiterals(final Expression expression) {
        return asList(expression.getValue().split(","));
    }

    /**
     * Prints variable on the screen.
     *
     * @param literals   represents parsed Expression`s value
     * @param expression represents one specific Expression
     * @param index      number of the specific line of the source code
     * @throws SyntaxErrorException when variable does not exists
     */
    private void writeVariable(final List<String> literals,
                               final Expression expression,
                               final int index) throws SyntaxErrorException {
        int result = ofNullable(this.variables.get(literals.get(1))).orElseThrow(
                () -> new SyntaxErrorException(
                        format("Syntax error on line %s, near '%s'. The variable %s does not exists!",
                               index,
                               expression.getValue(),
                               literals.get(1))));

        System.out.println(format("Variable '%s' has value %s", literals.get(1), result));
    }

    /**
     * Reads value and stores it in variable. If the variable is not present creates new one,
     * otherwise overwrites the old value.
     *
     * @param literals represents parsed Expression`s value
     */
    private void readVariable(final List<String> literals) {
        System.out.println("Please provide a variable: ");
        Scanner scanner = new Scanner(this.in);
        String input = scanner.nextLine();

        this.variables.put(literals.get(1), getIntValueOrThrow(input));
    }

    /**
     * Provides specific operation according to Expression`s keyword. If the variable is not present
     * creates new one, otherwise overwrites the old value.
     *
     * @param literals   represents parsed Expression`s value
     * @param expression represents one specific Expression
     */
    private void provideOperation(final List<String> literals, final Expression expression) {
        Optional<Integer> arg1 = ofNullable(this.variables.get(literals.get(1)));
        Optional<Integer> arg2 = ofNullable(this.variables.get(literals.get(2)));

        int result = expression.getOperation()
                               .apply(arg1.orElseGet(() -> getIntValueOrThrow(literals.get(1))),
                                      arg2.orElseGet(() -> getIntValueOrThrow(literals.get(2))));
        this.variables.put(literals.get(3), result);
    }

    /**
     * Assigns the value to variable.
     *
     * @param literals represents parsed Expression`s value
     */
    private void assignVariable(final List<String> literals) {
        Optional<Integer> arg = ofNullable(this.variables.get(literals.get(2)));

        this.variables.put(literals.get(1),
                           arg.orElseGet(() -> getIntValueOrThrow(literals.get(2))));
    }

    /**
     * Performs jump operation in dependence on the condition.
     *
     * @param literals      represents parsed Expression`s value
     * @param shouldEqualTo tells if condition should be true or false
     * @param length        represents the length of the processed list
     * @param index         number of the specific line of the source code
     * @return new index if, and only if conditions are met, otherwise return old index
     * @throws SyntaxErrorException when the result is out of range
     */
    private int provideJump(final List<String> literals,
                            final boolean shouldEqualTo,
                            final int length,
                            final int index) throws SyntaxErrorException {
        Optional<Integer> arg = ofNullable(this.variables.get(literals.get(1)));
        int condition = arg.orElseGet(() -> getIntValueOrThrow(literals.get(1)));

        int jumpValue = getIntValueOrThrow(literals.get(2));

        if (condition > 0 == shouldEqualTo) {
            if (jumpValue - 1 > length - 1) { // -1 because the list starts at 0 index
                throw new SyntaxErrorException(
                        format("Syntax error on line %s. The line number %s is out of range.",
                               index + 1,
                               jumpValue - 1));
            }
            return jumpValue - 2; // -1 because of for loop and another -1 because the list starts at 0 index
        } else {
            return index;
        }
    }

    /**
     * Tries to obtain integer value from the input string if is it possible,
     * otherwise throws exception.
     *
     * @param literal represents parsed Expression`s value
     * @return integer value obtained from the input string
     * @throws SyntaxErrorException when the input string cannot be converted into integer value
     */
    private int getIntValueOrThrow(final String literal) throws SyntaxErrorException {
        try {
            return parseInt(literal);
        } catch (NumberFormatException e) {
            throw new SyntaxErrorException(
                    format("Syntax error. The literal '%s' is neither variable, nor number.", literal), e);
        }
    }
}
