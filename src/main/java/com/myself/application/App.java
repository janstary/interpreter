package com.myself.application;

import com.myself.application.interpreter.impl.DefaultInterpreter;

public class App {

    public static void main(String[] args) {
        try {
            DefaultInterpreter.of(args[0])
                              .interpret();
        } catch (ArrayIndexOutOfBoundsException e) {
            System.err.println("No file was selected!");
        }
    }
}
