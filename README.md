#Interpreter

##Problem

- Input of the program is text file consist of specific instructions described later in section syntax.

- The input of the program is:
  - text file consist of specific instructions described later in section syntax.
  - or values inserted by user (READ instruction)

- The output of the program is:
  - console output (WRITE instruction).
  - or error statement

- The file with instructions is input argument of the program

##Syntax

Instruction   | Description
------------- | -------------
READ,i        | The program reads the number from the keyboard and stores it in the i variable.
WRITE,i       | The program writes the contents of the variable to the screen.
+,i,j,k       | The program calculates the sum of i + j and stores the result in the variable k.
-,i,j,k       | The program calculates the difference i - j and stores the result in the variable k.
*,i,j,k       | The program calculates the product i ∗ j and stores the result in the variable k.
<,i,j,k       | The program inserts the result of the comparison i < j into the variable.
>,i,j,k       | The program inserts the result of the comparison i > j into the variable.
>=,i,j,k      | The program puts the comparison result i ≥ j in the variable k.
<=,i,j,k      | The program inserts i ≤ j into the variable for the result of the comparison.
==,i,j,k      | The program inserts the i == j result into the variable k.
=,i,j         | The program assigns j to the variable i.
JUMP,r        | The program jumps to line r in the instruction file and continues from it.
JUMPT,i,r     | If true (TRUE), the program jumps to line r in the instruction file and continues from it.
JUMPF,i,r     | If the value is false (FALSE), the program jumps to line r in the instruction file and continues from it.
NOP           | Nothing is done and the interpreter continues

##Build the program
```bash
mvn clean install
```

##Test the program
```bash
mvn test
```

### Run the program
```bash
cd target\
java -jar .\interpreter-1.0-SNAPSHOT.jar file
```
